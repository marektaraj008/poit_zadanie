from threading import Lock
from flask import Flask, render_template, session, request, jsonify, url_for
from flask_socketio import SocketIO, emit, disconnect      
import math
import time
import configparser as ConfigParser
import random
import serial
import os
import json

async_mode = None

app = Flask(__name__)



app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock() 

ser= serial.Serial("/dev/ttyUSB0")
ser.baudrate = 9600

def background_thread(args):
    count = 0  
    dataCounter = 0 
    dataList = []  
    
    while True:
        if args:
          A = dict(args).get('A')
          dbV = dict(args).get('db_value')         
        else:
          A = 1
          dbV = 'nieco'  
        print(args)
        if (dict(args).get('A') == None):
            command = '0'
        else:
            command = str(dict(args).get('A'));
        print(command)
        socketio.sleep(2)
        count += 1
        dataCounter +=1
        data = 0                   
        
        
        
        #ser.write(command.encode())
        if dbV == 'start':
            command = '1';
            ser.write(command.encode())
            data = ser.readline()
            data = data.decode()   
            dataDict = {            
            "t": data}
            dataList.append(dataDict)
             
        else:
          command = '2';
          ser.write(command.encode())  
          if len(dataList)>0:
            print(str(dataList))            
            fuj = str(dataList).replace("'", "\"")
            print(fuj)
            
          dataList = []
          dataCounter = 0
        if data != 0:
            socketio.emit('my_response', {'datat': data}, namespace='/test')
            write_to_file(count, data) 
    
def write_to_file(index, data):    
    file = open("static/test.txt","a+")
    row = json.dumps({"index":index, "value":data})
    file.write("%s\r\n" %row)
    

@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)

@app.route('/graph', methods=['GET', 'POST'])
def graph():
    return render_template('graph.html', async_mode=socketio.async_mode)
    
@app.route('/db')
def db():
  db = MySQLdb.connect(host=myhost,user=myuser,passwd=mypasswd,db=mydb)
  cursor = db.cursor()
  cursor.execute('''SELECT  hodnoty FROM  graph WHERE id=1''')
  rv = cursor.fetchall()
  return str(rv)    

@app.route('/dbdata/<string:num>', methods=['GET', 'POST'])
def dbdata(num):
  db = MySQLdb.connect(host=myhost,user=myuser,passwd=mypasswd,db=mydb)
  cursor = db.cursor()
  print(num)
  cursor.execute("SELECT hodnoty FROM  graph WHERE id=%s", num)
  rv = cursor.fetchone()
  return str(rv[0])
    
@socketio.on('my_event', namespace='/test')
def test_message(message):   
    session['receive_count'] = session.get('receive_count', 0) + 1 
    session['A'] = message['value']    
    emit('my_response',
         {'data': message['value'], 'count': session['receive_count']})
    

@socketio.on('db_event', namespace='/test')
def db_message(message):   
#    session['receive_count'] = session.get('receive_count', 0) + 1 
    session['db_value'] = message['value']    
#    emit('my_response',
#         {'data': message['value'], 'count': session['receive_count']})

@socketio.on('disconnect_request', namespace='/test')
def disconnect_request():
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': 'Disconnected!', 'count': session['receive_count']})
    disconnect()

@socketio.on('connect', namespace='/test')
def test_connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(target=background_thread, args=session._get_current_object())
   # emit('my_response', {'data': 'Connected', 'count': 0})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected', request.sid)


if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", port=80, debug=True)
