#define OE A0
#define CE A1

#define DQ0 2
#define DQ1 3
#define DQ2 4
#define DQ3 5
#define DQ4 6
#define DQ5 7
#define DQ6 8
#define DQ7 9
#define DS 10
#define ST_CP 11
#define SH_CP 12

bool ready;

void setup() {
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo/Micro only
  }
  Serial.println("Begin");

  pinMode(OE, OUTPUT);
  pinMode(CE, OUTPUT);
  digitalWrite(OE, LOW);
  pinMode(DS, OUTPUT);
  pinMode(ST_CP, OUTPUT);
  pinMode(SH_CP, OUTPUT);
  pinMode(DQ0, INPUT);
  pinMode(DQ1, INPUT);
  pinMode(DQ2, INPUT);
  pinMode(DQ3, INPUT);
  pinMode(DQ4, INPUT);
  pinMode(DQ5, INPUT);
  pinMode(DQ6, INPUT);
  pinMode(DQ7, INPUT);
 
  Serial.println("Initial Pin Setting");
  delay(200);
  Serial.println("Setting OE Low");
  delay(200);
  Serial.println("Done initial setting"); 
}

void loop() { 
  while (Serial.available()){
    char in = Serial.read();
    if (in == '1') {
      ready = true;
    }
    else {
      ready = false;
    }
    getData(0);
  }
}

void getData(int inAddress) {
  int count = 0;
  int data[16];
  delay(500);
  Serial.println("Beginning address iteration");
  if (ready) {
    for (unsigned int addr=inAddress; addr<=65535; addr++){

      char in_char = Serial.read();
      if (in_char == '2') {    
        addr=65535;
      }
      
      setAddress(addr);
      
      // Serial.println("Setting CE High");
      digitalWrite(CE, HIGH);
      // Serial.println("Reading Data");
      int in[8];
     
      // Serial.println("Reading DQ0-DQ7");
      // Little-Endian
      in[7] = digitalRead(DQ7);
      in[6] = digitalRead(DQ6);
      in[5] = digitalRead(DQ5);
      in[4] = digitalRead(DQ4);
      in[3] = digitalRead(DQ3);
      in[2] = digitalRead(DQ2);
      in[1] = digitalRead(DQ1);
      in[0] = digitalRead(DQ0); 
      
      int test=0;
      test += in[7]*pow(2,7);
      test += in[6]*pow(2,6);
      test += in[5]*pow(2,5);
      test += in[4]*pow(2,4);
      test += in[3]*pow(2,3);
      test += in[2]*pow(2,2);
      test += in[1]*pow(2,1);
      test += in[0]*pow(2,0);
  
      // Serial.println("Setting CE Low");
      digitalWrite(CE, LOW); 
      if (count <=15){          
        data[count] = test;
        count++;     
        }
      else {
        char buf[80];
  //      
        sprintf(buf, "%u: %03d %03d %03d %03d %03d %03d %03d %03d %03d %03d %03d %03d %03d %03d %03d %03d",
                addr/16 ,data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7],
                data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15]);
        Serial.println(buf);
        
        //delay(5);
        for (int j=0; j<=15; j++) {
          data[j] = 0;
          }
        count = 0;
        }
     }          
  }
  Serial.println("Done!");
  
}

void setAddress(int address) {
  shiftOut(DS, SH_CP, MSBFIRST, (address >> 8));
  shiftOut(DS, SH_CP, MSBFIRST, address);
//  for(int i=0; i<=4; i++){
//    digitalWrite(DS, HIGH);
//    digitalWrite(SH_CP, LOW);
//    digitalWrite(SH_CP,HIGH);
//    digitalWrite(SH_CP, LOW);
//    digitalWrite(DS, LOW);
//    digitalWrite(SH_CP, LOW);
//    digitalWrite(SH_CP,HIGH);
//    digitalWrite(SH_CP, LOW);
//    }
  
  digitalWrite(ST_CP, LOW);
  digitalWrite(ST_CP, HIGH);
  delay(2);
  digitalWrite(ST_CP, LOW);
  }
